package amazon.common.exception


class UninitializeProductsException : RuntimeException("Trying to access Products node, without having initialized it before") {}