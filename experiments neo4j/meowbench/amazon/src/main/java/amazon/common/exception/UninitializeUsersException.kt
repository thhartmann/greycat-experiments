package amazon.common.exception


class UninitializeUsersException : RuntimeException("Trying to access Users node, without having initialized it before") {}