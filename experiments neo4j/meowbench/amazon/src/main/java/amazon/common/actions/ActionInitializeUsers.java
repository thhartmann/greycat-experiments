package amazon.common.actions;

import amazon.common.task.MainNodesTasks;
import greycat.*;
import greycat.plugin.SchedulerAffinity;
import greycat.struct.Buffer;

public class ActionInitializeUsers implements Action {
    public void eval(final TaskContext ctx) {
        TaskResult tr = ctx.result();
        MainNodesTasks.initializeUsers()
                .executeFrom(
                        ctx, ctx.result(), SchedulerAffinity.SAME_THREAD,
                        new Callback<TaskResult>() {
                            public void on(TaskResult res) {
                                tr.free();
                                Exception exceptionDuringTask = null;
                                if (res != null) {
                                    if (res.output() != null) {
                                        ctx.append(res.output());
                                    }
                                    if (res.exception() != null) {
                                        exceptionDuringTask = res.exception();
                                    }
                                }
                                if (exceptionDuringTask != null) {
                                    ctx.endTask(res, exceptionDuringTask);
                                } else {
                                    ctx.continueWith(res);
                                }
                            }
                        });

    }

    public void serialize(Buffer builder) {
        builder.writeString(CommonAmazonActionNames.INITIALIZE_USERS);
        builder.writeChar(Constants.TASK_PARAM_OPEN);
        builder.writeChar(Constants.TASK_PARAM_CLOSE);
    }
}
