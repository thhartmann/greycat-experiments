package amazon.common.actions;

import amazon.common.exception.UninitializeUsersException;
import amazon.common.task.MainNodesTasks;
import greycat.*;
import greycat.plugin.SchedulerAffinity;
import greycat.struct.Buffer;

public class ActionRetrieveUsersMainNode implements Action {
    public void eval(final TaskContext ctx) {
        TaskResult tr = ctx.result();
        MainNodesTasks.retrieveUsersMainNode()
                .executeFrom(ctx, ctx.result(), SchedulerAffinity.SAME_THREAD,
                        new Callback<TaskResult>() {
                            public void on(TaskResult res) {
                                tr.free();
                                if (res != null) {
                                    if (res.size() == 0) {
                                        ctx.endTask(res, new UninitializeUsersException());
                                    } else {
                                        if (res.output() != null) {
                                            ctx.append(res.output());
                                        }
                                        ctx.continueWith(res);
                                    }
                                } else {
                                    ctx.endTask(res, new UninitializeUsersException());
                                }
                            }
                        });
    }

    public void serialize(Buffer builder) {
        builder.writeString(CommonAmazonActionNames.RETRIEVE_USERS_NODE);
        builder.writeChar(Constants.TASK_PARAM_OPEN);
        builder.writeChar(Constants.TASK_PARAM_CLOSE);

    }
}
