package amazon.common.actions;

import greycat.Action;

public class CommonAmazonActions {

    public static Action initializeProducts() {
        return new ActionInitializeProducts();
    }

    public static Action retrieveProductsMainNode() {
        return new ActionRetrieveProductsMainNode();
    }

    public static Action getOrCreateProduct(String productId) {
        return new ActionGetOrCreateProduct(productId);
    }

    public static Action initializeUsers() {
        return new ActionInitializeUsers();
    }

    public static Action retrieveUsersMainNode() {
        return new ActionRetrieveUsersMainNode();
    }

    public static Action getUserById(String userId) {
        return new ActionGetUserById(userId);
    }
}
