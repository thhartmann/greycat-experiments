package amazon.common.task

import amazon.AmazonConstants
import amazon.AmazonConstants.*
import greycat.*
import greycat.Constants.BEGINNING_OF_TIME
import greycat.Tasks.newTask
import mylittleplugin.MyLittleActions.*


object MainNodesTasks {
    @JvmStatic
    fun initializeProducts(): Task {
        return newTask()
                .readGlobalIndex(ENTRY_POINT_INDEX, ENTRY_POINT_NODE_NAME, PRODUCTS_NODE_NAME)
                .then(ifEmptyThen(
                        newTask().then(executeAtWorldAndTime("0", "${Constants.BEGINNING_OF_TIME}",
                                newTask()
                                        .createNode()
                                        .setAttribute(ENTRY_POINT_NODE_NAME, Type.STRING, PRODUCTS_NODE_NAME)
                                        .timeSensitivity("-1", "0")
                                        .addToGlobalIndex(ENTRY_POINT_INDEX, ENTRY_POINT_NODE_NAME)
                        ))
                ))
    }

    @JvmStatic
    fun initializeUsers(): Task {
        return newTask()
                .readGlobalIndex(ENTRY_POINT_INDEX, ENTRY_POINT_NODE_NAME, USERS_NODE_NAME)
                .then(ifEmptyThen(
                        newTask().then(executeAtWorldAndTime("0", "$BEGINNING_OF_TIME",
                                Tasks.newTask()
                                        .createNode()
                                        .setAttribute(AmazonConstants.ENTRY_POINT_NODE_NAME, Type.STRING, AmazonConstants.USERS_NODE_NAME)
                                        .timeSensitivity("-1", "0")
                                        .addToGlobalIndex(AmazonConstants.ENTRY_POINT_INDEX, AmazonConstants.ENTRY_POINT_NODE_NAME)
                        ))
                ))
    }


    @JvmStatic
    fun retrieveProductsMainNode(): Task {
        return newTask().
                readGlobalIndex(ENTRY_POINT_INDEX, ENTRY_POINT_NODE_NAME, PRODUCTS_NODE_NAME)
    }

    @JvmStatic
    fun retrieveUsersMainNode(): Task {
        return newTask()
                .readGlobalIndex(ENTRY_POINT_INDEX, ENTRY_POINT_NODE_NAME, USERS_NODE_NAME)
    }

}