package amazon.common.task

import amazon.AmazonConstants.*
import amazon.common.actions.CommonAmazonActions.retrieveProductsMainNode
import greycat.*
import greycat.Constants.BEGINNING_OF_TIME
import greycat.Tasks.newTask
import mylittleplugin.MyLittleActions.*


object ProductTasks {

    @JvmStatic
    fun getOrCreateProduct(productId: String): Task {
        return newTask()
                .then(retrieveProductsMainNode())
                .setAsVar("productsNode")
                .traverse(PRODUCTS_RELATION, PRODUCT_ID, productId)
                .then(ifEmptyThen(
                        newTask()
                                .then(executeAtWorldAndTime("0", "$BEGINNING_OF_TIME",
                                newTask()
                                        .createNode()
                                        .setAttribute(PRODUCT_ID, Type.STRING, productId)
                                        .timeSensitivity("-1", "0")
                                        .setAsVar("newProduct")
                                        .readVar("productsNode")
                                        .addVarToRelation(PRODUCTS_RELATION, "newProduct", PRODUCT_ID)
                                        .readVar("newProduct")
                        )
                        )
                ))
    }
}