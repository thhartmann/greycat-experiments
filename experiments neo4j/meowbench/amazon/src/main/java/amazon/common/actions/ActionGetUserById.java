package amazon.common.actions;

import amazon.common.task.UserTasks;
import greycat.*;
import greycat.internal.task.TaskHelper;
import greycat.plugin.SchedulerAffinity;
import greycat.struct.Buffer;

import static amazon.common.actions.CommonAmazonActionNames.GET_USER_BY_ID;

public class ActionGetUserById implements Action {
    private final String _id;

    public ActionGetUserById(String p_id) {
        this._id = p_id;
    }

    @Override
    public void eval(TaskContext ctx) {
        TaskResult tr = ctx.result();
        UserTasks.getUserById(_id)
                .executeFrom(ctx, ctx.result(), SchedulerAffinity.SAME_THREAD,
                        new Callback<TaskResult>() {
                            public void on(TaskResult res) {
                                Exception exceptionDuringTask = null;
                                tr.free();
                                if (res != null) {
                                    if (res.output() != null) {
                                        ctx.append(res.output());
                                    }
                                    if (res.exception() != null) {
                                        exceptionDuringTask = res.exception();
                                    }
                                }
                                if (exceptionDuringTask != null) {
                                    ctx.endTask(res, exceptionDuringTask);
                                } else {
                                    ctx.continueWith(res);
                                }
                            }
                        });
    }

    @Override
    public void serialize(Buffer builder) {
        builder.writeString(GET_USER_BY_ID);
        builder.writeChar(Constants.TASK_PARAM_OPEN);
        TaskHelper.serializeString(_id, builder, true);
        builder.writeChar(Constants.TASK_PARAM_CLOSE);
    }
}
