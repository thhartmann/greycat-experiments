package amazon.common.actions;

import amazon.common.exception.UninitializeProductsException;
import amazon.common.task.MainNodesTasks;
import greycat.*;
import greycat.plugin.SchedulerAffinity;
import greycat.struct.Buffer;

public class ActionRetrieveProductsMainNode implements Action{
    public void eval(final TaskContext ctx) {
        TaskResult tr = ctx.result();
        MainNodesTasks.retrieveProductsMainNode()
                .executeFrom(ctx, ctx.result(), SchedulerAffinity.SAME_THREAD,
                        new Callback<TaskResult>() {
                            public void on(TaskResult res) {
                                tr.free();
                                if (res != null) {
                                    if (res.size() == 0) {
                                        ctx.endTask(res, new UninitializeProductsException());
                                    } else {
                                        if (res.output() != null) {
                                            ctx.append(res.output());
                                        }
                                        ctx.continueWith(res);
                                    }
                                } else {
                                    ctx.endTask(res, new UninitializeProductsException());
                                }
                            }
                        });
    }

    public void serialize(Buffer builder) {
        builder.writeString(CommonAmazonActionNames.RETRIEVE_PRODUCTS_NODE);
        builder.writeChar(Constants.TASK_PARAM_OPEN);
        builder.writeChar(Constants.TASK_PARAM_CLOSE);

    }
}
