package amazon.common.task

import amazon.AmazonConstants.*
import amazon.common.actions.CommonAmazonActions.retrieveUsersMainNode
import greycat.Task
import greycat.Tasks.newTask


object UserTasks {

    @JvmStatic
    fun getUserById(id: String): Task {
        return newTask()
                .then(retrieveUsersMainNode())
                .traverse(USER_RELATION, USER_ID, id)
    }
}