package amazon.common.actions;

import amazon.common.task.ProductTasks;
import greycat.*;
import greycat.internal.task.TaskHelper;
import greycat.plugin.SchedulerAffinity;
import greycat.struct.Buffer;

public class ActionGetOrCreateProduct implements Action {

    private final String _id;

    public ActionGetOrCreateProduct(String p_id) {
        this._id = p_id;
    }


    @Override
    public void eval(TaskContext ctx) {
        TaskResult tr = ctx.result();
        ProductTasks.getOrCreateProduct(_id)
                .executeFrom(ctx, ctx.result(), SchedulerAffinity.SAME_THREAD,
                        new Callback<TaskResult>() {
                            public void on(TaskResult res) {
                                tr.free();
                                Exception exceptionDuringTask = null;
                                if (res != null) {
                                    if (res.output() != null) {
                                        ctx.append(res.output());
                                    }
                                    if (res.exception() != null) {
                                        exceptionDuringTask = res.exception();
                                    }
                                }
                                if (exceptionDuringTask != null) {
                                    ctx.endTask(res, exceptionDuringTask);
                                } else {
                                    ctx.continueWith(res);
                                }
                            }
                        });
    }

    @Override
    public void serialize(Buffer builder) {
        builder.writeString(CommonAmazonActionNames.GET_OR_CREATE_PRODUCT);
        builder.writeChar(Constants.TASK_PARAM_OPEN);
        TaskHelper.serializeString(_id, builder, true);
        builder.writeChar(Constants.TASK_PARAM_CLOSE);
    }
}
