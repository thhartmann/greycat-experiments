package amazon.common.actions;

public class CommonAmazonActionNames {
    public static String INITIALIZE_PRODUCTS = "initializeProducts";
    public static String INITIALIZE_USERS = "initializeUsers";

    public static String RETRIEVE_PRODUCTS_NODE = "retrieveProductsNode";
    public static String RETRIEVE_USERS_NODE = "retrieveUsersNode";

    public static String GET_OR_CREATE_PRODUCT = "getOrCreateProduct";

    public static String GET_USER_BY_ID = "getUserById";


}
