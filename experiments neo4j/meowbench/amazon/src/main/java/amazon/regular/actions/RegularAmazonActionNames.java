package amazon.regular.actions;

public class RegularAmazonActionNames {

    public static String GET_OR_CREATE_USER = "getOrCreateUserNT";

    public static String GET_USER_BY_NAME = "getUserByNameNT";

    public static String ADD_REVIEW = "addReviewNT";
}
