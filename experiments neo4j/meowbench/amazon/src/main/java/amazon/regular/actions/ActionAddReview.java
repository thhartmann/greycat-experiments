package amazon.regular.actions;

import amazon.regular.task.ReviewTasks;
import greycat.*;
import greycat.internal.task.TaskHelper;
import greycat.plugin.SchedulerAffinity;
import greycat.struct.Buffer;

import static amazon.regular.actions.RegularAmazonActionNames.ADD_REVIEW;

public class ActionAddReview implements Action {


    private final String _pid;
    private final String _uid;
    private final String _profileName;
    private final String _helpfulnessIn;
    private final String _helpfulnessOut;
    private final String _score;
    private final String _time;
    private final String _summary;
    private final String _text;

    public ActionAddReview(String pid,
                           String uid,
                           String profileName,
                           String helpfulnessIn,
                           String helpfulnessOut,
                           String score,
                           String time,
                           String summary,
                           String text) {
        this._pid = pid;
        this._uid = uid;
        this._profileName = profileName;
        this._helpfulnessIn = helpfulnessIn;
        this._helpfulnessOut = helpfulnessOut;
        this._score = score;
        this._time = time;
        this._summary = summary;
        this._text = text;
    }

    @Override
    public void eval(TaskContext ctx) {
        TaskResult tr = ctx.result();
        String pid = (String) ctx.variable(ctx.template(_pid)).get(0);
        String uid = (String) ctx.variable(ctx.template(_uid)).get(0);
        String profileName = (String) ctx.variable(ctx.template(_profileName)).get(0);
        int helpfulnessIn = (int) ctx.variable(ctx.template(_helpfulnessIn)).get(0);
        int helpfulnessOut = (int) ctx.variable(ctx.template(_helpfulnessOut)).get(0);
        double score = (double) ctx.variable(ctx.template(_score)).get(0);
        long time = (long) ctx.variable(ctx.template(_time)).get(0);
        String summary = (String) ctx.variable(ctx.template(_summary)).get(0);
        String text = (String) ctx.variable(ctx.template(_text)).get(0);
        ReviewTasks.addReview(pid, uid, profileName, helpfulnessIn, helpfulnessOut, score, time, summary, text)
                .executeFrom(ctx, ctx.result(), SchedulerAffinity.SAME_THREAD,
                        new Callback<TaskResult>() {
                            public void on(TaskResult res) {
                                tr.free();
                                Exception exceptionDuringTask = null;
                                if (res != null) {
                                    if (res.output() != null) {
                                        ctx.append(res.output());
                                    }
                                    if (res.exception() != null) {
                                        exceptionDuringTask = res.exception();
                                    }
                                }
                                if (exceptionDuringTask != null) {
                                    ctx.endTask(res, exceptionDuringTask);
                                } else {
                                    ctx.continueWith(res);
                                }
                            }
                        });
    }

    @Override
    public void serialize(Buffer builder) {
        builder.writeString(ADD_REVIEW);
        builder.writeChar(Constants.TASK_PARAM_OPEN);
        TaskHelper.serializeString(_pid, builder, true);
        builder.writeChar(Constants.TASK_PARAM_SEP);
        TaskHelper.serializeString(_uid, builder, true);
        builder.writeChar(Constants.TASK_PARAM_SEP);
        TaskHelper.serializeString(_profileName, builder, true);
        builder.writeChar(Constants.TASK_PARAM_SEP);
        TaskHelper.serializeString(_helpfulnessIn, builder, true);
        builder.writeChar(Constants.TASK_PARAM_SEP);
        TaskHelper.serializeString(_helpfulnessOut, builder, true);
        builder.writeChar(Constants.TASK_PARAM_SEP);
        TaskHelper.serializeString(_score, builder, true);
        builder.writeChar(Constants.TASK_PARAM_SEP);
        TaskHelper.serializeString(_time, builder, true);
        builder.writeChar(Constants.TASK_PARAM_SEP);
        TaskHelper.serializeString(_summary, builder, true);
        builder.writeChar(Constants.TASK_PARAM_SEP);
        TaskHelper.serializeString(_text, builder, true);
        builder.writeChar(Constants.TASK_PARAM_CLOSE);
    }
}
