package amazon.regular.actions;

import amazon.regular.task.UserTasks;
import greycat.*;
import greycat.internal.task.TaskHelper;
import greycat.plugin.SchedulerAffinity;
import greycat.struct.Buffer;

public class ActionGetUserByName implements Action {
    private final String _name;

    public ActionGetUserByName(String p_name) {
        this._name = p_name;
    }


    @Override
    public void eval(TaskContext ctx) {
        TaskResult tr = ctx.result();
        UserTasks.getUserByName(_name)
                .executeFrom(ctx, ctx.result(), SchedulerAffinity.SAME_THREAD,
                        new Callback<TaskResult>() {
                            public void on(TaskResult res) {
                                tr.free();
                                Exception exceptionDuringTask = null;
                                if (res != null) {
                                    if (res.output() != null) {
                                        ctx.append(res.output());
                                    }
                                    if (res.exception() != null) {
                                        exceptionDuringTask = res.exception();
                                    }
                                }
                                if (exceptionDuringTask != null) {
                                    ctx.endTask(res, exceptionDuringTask);
                                } else {
                                    ctx.continueWith(res);
                                }
                            }
                        });
    }

    @Override
    public void serialize(Buffer builder) {
        builder.writeString(RegularAmazonActionNames.GET_USER_BY_NAME);
        builder.writeChar(Constants.TASK_PARAM_OPEN);
        TaskHelper.serializeString(_name, builder, true);
        builder.writeChar(Constants.TASK_PARAM_CLOSE);
    }
}
