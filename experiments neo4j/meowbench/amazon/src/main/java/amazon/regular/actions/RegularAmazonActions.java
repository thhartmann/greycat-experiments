package amazon.regular.actions;

import greycat.Action;

public class RegularAmazonActions {

    public static Action getUserByName(String name) {
        return new ActionGetUserByName(name);
    }

    public static Action getOrCreateUser(String userId, String userName) {
        return new ActionGetOrCreateUser(userId, userName);
    }

    public static Action addReview(String pid,
                                   String uid,
                                   String profileName,
                                   String helpfulnessIn,
                                   String helpfulnessOut,
                                   String score,
                                   String time,
                                   String summary,
                                   String text) {
        return new ActionAddReview(pid, uid, profileName, helpfulnessIn, helpfulnessOut, score, time, summary, text);
    }
}
