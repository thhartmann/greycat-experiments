package amazon.regular.actions;

import amazon.regular.task.UserTasks;
import greycat.*;
import greycat.internal.task.TaskHelper;
import greycat.plugin.SchedulerAffinity;
import greycat.struct.Buffer;

public class ActionGetOrCreateUser implements Action {
    private final String _id;
    private final String _name;

    public ActionGetOrCreateUser(String p_id, String p_name) {
        this._id = p_id;
        this._name = p_name;
    }


    @Override
    public void eval(TaskContext ctx) {
        TaskResult tr = ctx.result();
        UserTasks.getOrCreateUser(_id,_name)
                .executeFrom(ctx, ctx.result(), SchedulerAffinity.SAME_THREAD,
                        new Callback<TaskResult>() {
                            public void on(TaskResult res) {
                                tr.free();
                                Exception exceptionDuringTask = null;
                                if (res != null) {
                                    if (res.output() != null) {
                                        ctx.append(res.output());
                                    }
                                    if (res.exception() != null) {
                                        exceptionDuringTask = res.exception();
                                    }
                                }
                                if (exceptionDuringTask != null) {
                                    ctx.endTask(res, exceptionDuringTask);
                                } else {
                                    ctx.continueWith(res);
                                }
                            }
                        });
    }

    @Override
    public void serialize(Buffer builder) {
        builder.writeString(RegularAmazonActionNames.GET_OR_CREATE_USER);
        builder.writeChar(Constants.TASK_PARAM_OPEN);
        TaskHelper.serializeString(_id, builder, true);
        builder.writeChar(Constants.TASK_PARAM_SEP);
        TaskHelper.serializeString(_name, builder, true);
        builder.writeChar(Constants.TASK_PARAM_CLOSE);
    }
}
