package amazon.regular.task

import amazon.AmazonConstants.*
import amazon.common.actions.CommonAmazonActions.*
import greycat.*
import greycat.Constants.BEGINNING_OF_TIME
import greycat.Tasks.newTask
import mylittleplugin.MyLittleActions.*


object UserTasks {
    @JvmStatic
    fun getUserByName(name: String): Task {
        return newTask()
                .then(retrieveUsersMainNode())
                .traverse(USER_RELATION)
                .selectWith(USER_NAME, name)
    }



    @JvmStatic
    fun getOrCreateUser(id: String, name: String): Task {
        return newTask()
                .then(getUserById(id))
                .then(ifEmptyThen(
                        newTask()
                                .then(executeAtWorldAndTime("0", "$BEGINNING_OF_TIME",
                                newTask()
                                        .createNode()
                                        .setAttribute(USER_ID, Type.STRING, id)
                                        .setAttribute(USER_NAME, Type.STRING, name)
                                        .timeSensitivity("-1", "0")
                                        .setAsVar("newUser")
                                        .then(retrieveUsersMainNode())
                                        .addVarToRelation(USER_RELATION, "newUser", USER_ID)
                                        .readVar("newUser")
                        )
                        )
                )
                )
    }
}