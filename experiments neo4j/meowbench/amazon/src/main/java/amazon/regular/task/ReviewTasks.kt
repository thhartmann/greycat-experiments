package amazon.regular.task

import amazon.AmazonConstants.*
import amazon.common.actions.CommonAmazonActions
import amazon.regular.actions.RegularAmazonActions
import greycat.*
import greycat.Tasks.newTask
import mylittleplugin.MyLittleActions.ifEmptyThen

object ReviewTasks {

    @JvmStatic
    fun addReview(
            pid: String,
            uid: String,
            profileName: String,
            helpfulnessIn: Int,
            helpfulnessOut: Int,
            score: Double,
            time: Long,
            summary: String,
            text: String
    ): Task {

        return newTask()
                .travelInTime("$time")
                .then(RegularAmazonActions.getOrCreateUser(uid, profileName))
                .thenDo { ctx ->
                    ctx.setVariable("uid", "[${ctx.resultAsNodes()[0].id()}]")
                    ctx.continueTask()
                }
                .setAsVar("user")
                .then(CommonAmazonActions.getOrCreateProduct(pid))
                .setAsVar("product")
                .traverse(PRODUCT_TO_REVIEW_REL, REVIEW_USER, "{{uid}}")
                .then(ifEmptyThen(
                        newTask()
                                .createNode()
                                .setAttribute("helpfulness", Type.INT_ARRAY, "[$helpfulnessIn,$helpfulnessOut]")
                                .setAttribute("score", Type.DOUBLE, "$score")
                                .setAttribute("summary", Type.STRING, summary)
                                .setAttribute("text", Type.STRING, text)
                                .addVarToRelation(REVIEW_PRODUCT, "product")
                                .addVarToRelation(REVIEW_USER, "user")
                                .setAsVar("newReview")
                                .readVar("user")
                                .addVarToRelation(USER_TO_REVIEW_REL, "newReview", REVIEW_PRODUCT)
                                .readVar("product")
                                .addVarToRelation(PRODUCT_TO_REVIEW_REL, "newReview", REVIEW_USER)
                                .readVar("newReview")
                ))
    }
}
