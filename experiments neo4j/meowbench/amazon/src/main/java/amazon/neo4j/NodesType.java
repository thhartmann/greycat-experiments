package amazon.neo4j;

import org.neo4j.graphdb.Label;

public enum NodesType implements Label {
    USER,
    REVIEW,
    PRODUCT;
}