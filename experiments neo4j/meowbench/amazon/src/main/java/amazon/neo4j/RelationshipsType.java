package amazon.neo4j;

import org.neo4j.graphdb.RelationshipType;

public enum RelationshipsType implements RelationshipType {
    USER_REVIEW,
    PRODUCT_REVIEW;
}
