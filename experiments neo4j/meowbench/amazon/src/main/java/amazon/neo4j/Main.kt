package amazon.neo4j

import org.neo4j.graphdb.*
import org.neo4j.graphdb.factory.GraphDatabaseFactory
import org.neo4j.graphdb.index.Index
import java.io.File
import java.nio.charset.StandardCharsets
import java.nio.file.*

object Main {


    @JvmStatic
    fun main(args: Array<String>) {

        addingContent("/Users/thomas/Downloads/movies.txt")

    }


    fun addingContent(urlMovies: String) {
        val starttime = System.currentTimeMillis()
        try {
            val dbFactory = GraphDatabaseFactory()
            val graph = dbFactory.newEmbeddedDatabase(File("/Users/thomas/papers/is2018/experiments/neo4j_db"))
            var tx: Transaction = graph.beginTx()
            val indexUser: Index<Node> = graph.index().forNodes("user")
            val indexProduct: Index<Node> = graph.index().forNodes("product")
            val path = Paths.get(urlMovies)
            val lines = Files.lines(path, StandardCharsets.ISO_8859_1)
            val sc = lines.iterator()
            var i = 0
            while (sc.hasNext()) {
                var pid = ""
                var uid = ""
                var profileName = ""
                var helpfulnessIn = 0
                var helpfulnessOut = 0
                var score: Double = 0.0
                var time: Long = 0
                var summary = ""
                var text = ""
                for (i in 0..8) {
                    var line = sc.next()
                    when (i) {
                        0 -> {
                            pid = regexpProductId.find(line)!!.groups.get(1)!!.value
                        }
                        1 -> {
                            uid = regexpUserId.find(line)!!.groups[1]!!.value
                        }
                        2 -> {
                            profileName = regexpProfile.find(line)!!.groups[1]!!.value
                        }
                        3 -> {
                            var find = regexpHelpfulness.find(line)

                            while (find == null) {

                                profileName += "\\n" + line
                                line = sc.next()
                                find = regexpHelpfulness.find(line)
//                                println("I'm Stuck in find")
                            }

                            helpfulnessIn = find.groups[1]!!.value.toInt()
                            helpfulnessOut = find.groups[2]!!.value.toInt()
                        }
                        4 -> {
                            score = regexpScore.find(line)!!.groups[1]!!.value.toDouble()
                        }
                        5 -> {
                            time = regexpTime.find(line)!!.groups[1]!!.value.toLong()
                        }
                        6 -> {
                            summary = regexpSummary.find(line)!!.groups[1]!!.value.replace("<br /><br />", " ").replace("<br />", " ")
                        }
                        7 -> {
                            var find = regexpText.find(line)

                            while (find == null) {
                                summary += "\\n" + line
                                line = sc.next()
                                find = regexpText.find(line)
//                                println("I'm Stuck in summary")
                            }
                            text = find.groups[1]!!.value.replace("<br /><br />", " ").replace("<br />", " ")
                        }
                        8 -> {
                            while (line.isNotEmpty()) {
                                text += "\\n" + line
                                line = sc.next()
//                                println("I'm Stuck in text")
                            }
                        }
                    }
                }
                val reviewNode = graph.createNode(NodesType.REVIEW)
                reviewNode.setProperty("helpfulnessIn", helpfulnessIn)
                reviewNode.setProperty("helpfulnessOut", helpfulnessOut)
                reviewNode.setProperty("score", score)
                reviewNode.setProperty("time", time)
                reviewNode.setProperty("summary", summary)
                reviewNode.setProperty("text", text)

                var product = indexProduct.get("pid", pid).single

                if (product == null) {
                    product = graph.createNode(NodesType.PRODUCT)
                    product.setProperty("pid", pid)
                    indexProduct.add(product, "pid", pid)

                }
                product.createRelationshipTo(reviewNode, RelationshipsType.PRODUCT_REVIEW)

                var user = indexUser.get("uid", uid).single
                if (user == null) {
                    user = graph.createNode(NodesType.USER)
                    user.setProperty("uid", uid)
                    user.setProperty("profileName", profileName)
                    indexUser.add(user, "uid", uid)
                }
                user.createRelationshipTo(reviewNode, RelationshipsType.USER_REVIEW)

                i++
                if (i % 10000 == 0) { // TODO save every ...
                    tx.success()
                    tx.close()
                    tx = graph.beginTx()
                }
                if (i % 100000 == 0) {
                    val currTime = System.currentTimeMillis()
                    println("for $i elements = ${currTime - starttime}")
                }
            }
            tx.success()
            tx.close()
            graph.shutdown()
        } finally {
            val currTime = System.currentTimeMillis()
            println("total Time = ${currTime - starttime}")
        }

    }

    val regexpProductId = Regex("product/productId: ([A-Za-z0-9]*)")
    val regexpUserId = Regex("review/userId: ([A-Za-z0-9]*)")
    val regexpProfile = Regex("review/profileName: (.*)")
    val regexpHelpfulness = Regex("review/helpfulness: ([0-9]*)/([0-9]*)")
    val regexpScore = Regex("review/score: ([0-9.]*)")
    val regexpTime = Regex("review/time: ([0-9]*)")
    val regexpSummary = Regex("review/summary: (.*)")
    val regexpText = Regex("review/text: (.*)")
}
