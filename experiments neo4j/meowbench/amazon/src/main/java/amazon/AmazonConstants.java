package amazon;

public class AmazonConstants {

    public final static String ENTRY_POINT_INDEX = "entryPoint";

    public final static String ENTRY_POINT_NODE_NAME = "name";

    public final static String PRODUCTS_NODE_NAME = "products";

    public final static String USERS_NODE_NAME = "users";


    public final static String PRODUCTS_RELATION = "productsList";

    public final static String PRODUCT_ID = "pid";

    public final static String USER_RELATION = "usersList";

    public final static String USER_ID = "uid";
    public final static String USER_NAME = "uname";

    public final static String PRODUCT_TO_REVIEW_REL = "reviews";
    public final static String USER_TO_REVIEW_REL = "reviews";

    public final static String REVIEW_PRODUCT = "product";
    public final static String REVIEW_USER = "user";
    public final static String REVIEW_SUMMARY= "summary";
    public final static String REVIEW_TEXT = "text";


    public final static String TOKENIZER = "default";


}
