package amazon

import amazon.AmazonConstants.PRODUCTS_RELATION
import amazon.common.actions.CommonAmazonActions.*
import amazon.regular.actions.RegularAmazonActions
import greycat.*
import greycat.Tasks.newTask
import greycat.rocksdb.RocksDBStorage
import greycat.scheduler.TrampolineScheduler
import mylittleplugin.*
import mylittleplugin.MyLittleActions.*
import java.nio.charset.StandardCharsets
import java.nio.file.*


object Main {


    @JvmStatic
    fun main(args: Array<String>) {

        val urlMovies = "/Users/thomas/Downloads/movies.txt"

        noTokenExp(urlMovies)


    }

    fun noTokenExp(urlMovies: String) {
        val graphNT = GraphBuilder()
                .withPlugin(MyLittleActionPlugin())
                .withStorage(RocksDBStorage("/Users/thomas/papers/is2018/experiments/gc_db"))
                .withMemorySize(10000000)
                .withScheduler(TrampolineScheduler())
                .build()



        graphNT.connect {
            val counter = graphNT.newCounter(1)
            addingContent(urlMovies, graphNT, false, 1000, counter)
            counter.then {
                graphNT.disconnect { }
            }
        }
    }


    fun addingContent(urlMovies: String, graph: Graph, meow: Boolean, saveEvery: Int, counter: DeferCounter) {
        val path = Paths.get(urlMovies)
        val lines = Files.lines(path, StandardCharsets.ISO_8859_1)
        val sc = lines.iterator()
        val timeStart = System.currentTimeMillis()
        newTask()
                .then(initializeUsers())
                .then(initializeProducts())
                .inject(0)
                .setAsVar("i")
                .whileDo(
                        { sc.hasNext() },
                        newTask()
                                .then(increment("i", 1))
                                .thenDo { ctx ->
                                    for (i in 0..8) {
                                        var line = sc.next()
                                        when (i) {
                                            0 -> {
                                                ctx.setVariable("pid", regexpProductId.find(line)!!.groups.get(1)!!.value)
                                            }
                                            1 -> {
                                                ctx.setVariable("uid", regexpUserId.find(line)!!.groups[1]!!.value)
                                            }
                                            2 -> {
                                                ctx.setVariable("profileName", regexpProfile.find(line)!!.groups[1]!!.value)
                                            }
                                            3 -> {
                                                var find = regexpHelpfulness.find(line)

                                                while (find == null) {
                                                    val profileName = ctx.variable("profileName")[0] as String
                                                    ctx.setVariable("profileName", profileName + "\\n" + line)
                                                    line = sc.next()
                                                    find = regexpHelpfulness.find(line)
//                                                    println("I'm Stuck in find")
                                                }

                                                ctx.setVariable("helpfulnessIn", find.groups[1]!!.value.toInt())
                                                ctx.setVariable("helpfulnessOut", find.groups[2]!!.value.toInt())
                                            }
                                            4 -> {
                                                ctx.setVariable("score", regexpScore.find(line)!!.groups[1]!!.value.toDouble())
                                            }
                                            5 -> {
                                                ctx.setVariable("time", regexpTime.find(line)!!.groups[1]!!.value.toLong())
                                            }
                                            6 -> {

                                                ctx.setVariable("summary", regexpSummary.find(line)!!.groups[1]!!.value.replace("<br /><br />", " ").replace("<br />", " "))
                                            }
                                            7 -> {
                                                var find = regexpText.find(line)

                                                while (find == null) {
                                                    val summary = ctx.variable("summary")[0] as String
                                                    ctx.setVariable("summary", summary + "\\n" + line)
                                                    line = sc.next()
                                                    find = regexpText.find(line)
//                                                    println("I'm Stuck in summary")
                                                }
                                                ctx.setVariable("text", find.groups[1]!!.value.replace("<br /><br />", " ").replace("<br />", " "))
                                            }
                                            8 -> {
                                                while (line.isNotEmpty()) {
                                                    val text = ctx.variable("text")[0] as String
                                                    ctx.setVariable("text", text + "\\n" + line)
                                                    line = sc.next()
//                                                    println("I'm Stuck in text")
                                                }
                                            }
                                        }
                                    }
                                    ctx.continueTask()
                                }
                                .ifThenElse({ meow },
                                        newTask(),
                                        newTask().then(RegularAmazonActions.addReview("pid", "uid", "profileName", "helpfulnessIn", "helpfulnessOut", "score", "time", "summary", "text"))
                                )
                                .ifThen(
                                        { ctx -> (ctx.variable("i")[0] as Int) % saveEvery == 0 },
                                        newTask()
                                                .thenDo { ctx ->
                                                    if ((ctx.variable("i")[0] as Int / saveEvery) % 100 == 0) {
                                                        val timeEnd = System.currentTimeMillis()
//                                                        println("saved ${ctx.variable("i")[0] as Int} in ${timeEnd - timeStart} ms")
                                                        println("${ctx.variable("i")[0] as Int} , ${timeEnd - timeStart}")

                                                    }
                                                    ctx.continueTask()
                                                }
                                                .save()
                                )
                )
                .then(retrieveProductsMainNode())
                .traverse(PRODUCTS_RELATION)
                .then(MyLittleActions.count())
                .println("{{result}}")
                .save()

                .execute(graph,
                        {
                            taskRes ->
                            val timeEnd = System.currentTimeMillis()
                            println("time to add everything: ${timeEnd - timeStart}")
                            counter.count()

                        })
    }

    val regexpProductId = Regex("product/productId: ([A-Za-z0-9]*)")
    val regexpUserId = Regex("review/userId: ([A-Za-z0-9]*)")
    val regexpProfile = Regex("review/profileName: (.*)")
    val regexpHelpfulness = Regex("review/helpfulness: ([0-9]*)/([0-9]*)")
    val regexpScore = Regex("review/score: ([0-9.]*)")
    val regexpTime = Regex("review/time: ([0-9]*)")
    val regexpSummary = Regex("review/summary: (.*)")
    val regexpText = Regex("review/text: (.*)")


}